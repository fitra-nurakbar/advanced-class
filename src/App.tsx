import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const [input, setInput] = useState({
    text: "",
  });

  const handlerChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const name = e.target.name;
    const value = e.target.value;

    setInput({ ...input, [name]: value });
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>{input.text}</h1>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <input
          type="text"
          name="text"
          onChange={handlerChange}
          placeholder="Input here..."
        />
      </header>
    </div>
  );
}

export default App;
